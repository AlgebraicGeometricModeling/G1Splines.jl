#Create a dictionary for INNER and BOUNDARY smoothing masks
using JLD2, FileIO
currentdir =  @__DIR__

include(joinpath(currentdir,"G1Splines.jl"));

D=Dict("CPNCS-AS"=>Dict(i=>G1matrix(i,"NCS-AS","EVREGBORDER") for i=3:20), "CPNCS-S"=>Dict(i=>G1matrix(i,"NCS-S","EVREGBORDER") for i=3:20), "CPCS-AS"=>Dict(i=>G1matrix(i,"CS-AS","EVREGBORDER") for i=3:20), "CPCS-S"=>Dict(i=>G1matrix(i,"CS-S","EVREGBORDER") for i=3:20), "CP"=>Dict(i=>acc3bigmatrix(i,"CORNER") for i=4), "BP"=>Dict(i=>acc3bigmatrix(i,"BORDER1") for i=4), "RV"=>Dict(i=>acc3bigmatrix(i,"INNEREV") for i=4), "CS-S"=>Dict(i=>G1matrix(i,"CS-S","INNER") for i=3:20), "CS-AS"=>Dict(i=>G1matrix(i,"CS-AS","INNER") for i=3:20), "NCS-S"=>Dict(i=>G1matrix(i,"NCS-S","INNER") for i=3:20), "NCS-AS"=>Dict(i=>G1matrix(i,"CS-S","INNER") for i=3:20),"BOUNDARY"=>Dict(i=>G1matrix(2*i,"CS-S","BORDEREV") for i=3:10));


save("EVsdict.jld2", "EVsdict",D);
