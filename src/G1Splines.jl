module G1Splines

using LinearAlgebra
using SemiAlgebraicTypes
using Axl
using DataStructures
using ToeplitzMatrices
using FileIO
using SparseArrays

#currentdir =  @__DIR__

G1S = Dict{Symbol, Any}( :pkgdir => dirname(dirname(pathof(G1Splines))) )

export G1S
    
#const MASKS=load(joinpath(currentdir,"EVsdict.jld2"))["EVsdict"];

const G1S[:masks] = Dict{String, Dict{Int64, Matrix{Float64}}}();

include("gspline.jl");
include("g1matrix.jl");
include("indexing.jl");
include("g1surface.jl");
include("g0surface.jl");

include("g1basis_functions.jl");

include("acc3bigmatrix.jl");
include("ordering.jl");
include("degree_elevate_mask.jl");
include("mesh_operations.jl");
include("mask_operations.jl");
include("order_bezier_cp.jl");

include("g1basis.jl");

include("g1basis_bezier.jl");

include("get_basis_from_sparse.jl")
include("g1dimension.jl");;

include("acc_d.jl");
include("eval.jl");
include("g1basis_assemble_spline.jl");

include("insert_knot.jl");

include("g1basis_gluingdata.jl");
include("g1basis_assemble_gluing.jl");

include("offread.jl");
include("gismo_output.jl");

include("fake_edges.jl")


end
