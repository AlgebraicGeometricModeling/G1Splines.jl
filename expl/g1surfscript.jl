"""
 Script to build the G1 surface
"""

using G1Splines, Axl

meshname = "rabbit.off";

#The input mesh must have EVs surrounded by just regular vertices; if this is not true, you must subdivide once (or more, for any reason).

if (size(ARGS,1)>0)
    m = offread(ARGS[1]);
else
    m = offdata(meshname);
end

sup = g1surface(m); #Collection of spline patches

@axlview sup #Plot the surface using Axl
