#This code generates the G1 Bézier biquintic basis functions on a domain parametrized by the G1ACC scheme 
#obtained from an input quad mesh and plot them in Axl

using G1Splines, Axl, SparseArrays


#The input mesh must have EVs surrounded by just regular vertices; if this is not true, the code subdivide it once (or more, for any reason).

meshname="triangle_planar.off";

if (size(ARGS,1)>0)
    m = offread(ARGS[1]);
else
    m = offdata(meshname)
end

gsurf = g1surface(m)
basis = g1basis(m)

for i in 1:size(basis)[2]
    b=basis[:,i]
    gsbas=get_basis_from_sparse(b,gsurf)
    @axlview gsbas
end
