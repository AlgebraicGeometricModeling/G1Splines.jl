"""
 Script to build the G0 surface
"""

using G1Splines, Axl

meshname = "cube.off";

if (size(ARGS,1)>0)
    m = offread(ARGS[1]);
else
    m = offdata(meshname);
end

sup = g0surface(m); #Collection of spline patches

@axlview sup #Plot the surface using Axl
