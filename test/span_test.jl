#This script tests if two sets of basis functions span the same spline space

include("../src/G1Splines.jl")


i=2; #nb subints
knt=knots(5,2^i,5);
m, d = dim_deg(knt);
N=3;

valances=[N 4]; #triangle case
basisker=g1basis_edges_kn(knt,valances);

subd_step=i;
m0=6;

basisexpl=spzeros(2*m^2,size(basisker,2))

global C=1;

for j in 1:2^subd_step -1

    glu0=[2*cos(2*pi/N),0,0];
    glu=glob_gluing_subd(glu0,subd_step);

    gluL=glu[j];
    gluR=glu[j+1];
    p=m0+(m0-1)*(j-1);


    basisexpl1=g1basis_noncrossingRV(knt, gluL, gluR,p,1,N)[:,1];
    basisexpl2=g1basis_noncrossingRV(knt, gluL, gluR,p,1,N)[:,4];

    u=[basisexpl1[loc_idx(m,3,1,1):loc_idx(m,3,m,m),:] ; basisexpl1[loc_idx(m,1,1,1):loc_idx(m,1,m,m),:] ];
    basisexpl[:,C]=u;
    global C+=1
    v=[basisexpl2[loc_idx(m,3,1,1):loc_idx(m,3,m,m),:] ; basisexpl2[loc_idx(m,1,1,1):loc_idx(m,1,m,m),:] ];
    basisexpl[:,C]=v;
    global C+=1
    #basisexplduo=hcat(u,v);
end

for i in 1:size(basisexpl,2)
    b=basisexpl[:,i]
    sol=Matrix(basisker)\b;
    println("Residual for the basis number " , i , ": " , norm(basisker*sol-b))
end

c=Matrix(basisker)\Matrix(basisexpl);

residual=norm(basisker*c-basisexpl);

println("Global residual: ", residual);
