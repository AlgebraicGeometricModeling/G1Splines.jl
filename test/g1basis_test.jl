include("../src/G1Splines.jl")
include("../src/g1dimension.jl")

#kn =[0.,0.,0.,0.,0.,0.         , 1., 1., 1., 1., 1., 1.]
#kn =[0.,0.,0.,0.,0.,0., 0.5, 0.5, 1., 1., 1., 1., 1., 1.]
N1 = 3; N2 = 4; mu=5

exact = false; true

for s in 3
    global ni = s; 2^s
    print("\n>>>> ni = ", ni, "  ")
    if exact
        global kn = knots(5,ni,mu, BigInt(0)//1,BigInt(1)//1);
    else
        global kn = knots(5,ni,mu);
    end


    #kn = knots(5,3,5)
    #kn = knots(5,4,5)
    #kn = knots(5,5,5)
    #kn = knots(5,6,5)
    #kn = knots(3,2,3)
    # m and dim of edge space for N2=4/3
    #kn = knots(3,2,2) #6    2/2 0
    #kn = knots(3,3,2) #8    4/4 0
    #kn = knots(3,4,2) #10   6/8 0/2
    #kn = knots(3,5,2) #12  8/10 2/2
    #kn = knots(3,6,2) #14 10/14 0/4
    #kn = knots(3,7,2) #16 12/16 0/4

    #kn = knots(4,2,3) #8    4/6 0
    #kn = knots(4,3,3) #11  8/10 1
    #kn = knots(4,4,3) #14 12/16 2
    #kn = knots(4,5,3)  #17 16/20 3

    #kn = knots(5,1,1) #6    2/2 0
    #kn = knots(5,2,1) #7    3/3 0
    #kn = knots(5,3,1) #8    4/4 0
    #kn = knots(5,4,1) #9    5/6 0
    #kn = knots(5,5,1) #10   6/6 0
    #kn = knots(5,6,1) #11   7/8 0

    #kn = knots(5,2,2) #8    4/6 0
    #kn = knots(5,3,2) #10   6/8 0
    #kn = knots(5,4,2) #12  8/12 0
    #kn = knots(5,5,2) #14 10/14 0

    #kn = knots(5,1,3) #9    6/8 0
    #kn = knots(5,2,3) #9    6/8 1
    #kn = knots(5,3,3) #12 10/12 2
    #kn = knots(5,4,3) #15 14/18 3
    #kn = knots(5,5,3) #18 14/18 4
    #kn = knots(5,6,3) #21 14/18 5
    #kn = knots(5,7,3) #21 14/18 6
    #kn = knots(5,8,3) #21 14/18 9
    #kn = knots(5,9,3) #21 14/18 12
    #kn = knots(5,10,3) #21 14/18 15
    #kn = knots(5,11,3) #21 14/18 18
    #kn = knots(5,12,3) #21 14/18 21
    #kn = knots(5,13,3) #21 14/18 24

    #kn = knots(5,2,4) #10  8/10 2
    #kn = knots(5,3,4) #14 13/16 3
    #kn = knots(5,4,4) #18 20/24 6


    #S0  = g1matrix_edge(kn,3,4);

    global S1, I1  = g1matrix_edge(kn,N1,N2,
                                   #zeroOnEdge=false,
                                   equalAcrossEdge=true,
                                   C1VirtualEdge=true
                                   );

    if exact
        global nwf = size(S1,2)-rankx(Matrix(S1))
    else
        global nwf = size(S1,2)-rank(Matrix(S1))
    end
    #B = nullspace(Matrix(S1))
    println("New edge functions: ",nwf, "  (exact=",exact,")")
end
#S2 = g1matrix_EV(kn,N1,[N2,N2,N2]; delta = nwf)
#vfcts = size(S2,2)-rank(Matrix(S2))
#println("Vertex functions: ",vfcts)
