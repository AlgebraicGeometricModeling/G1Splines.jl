include("../src/G1Splines.jl")
include("../src/g1dimension.jl")
using SemiAlgebraicTypes
N1 = 3; N2 = 4; mu=5

ni = 2 #number of sub-intervals

#kn = knots(5,ni,mu)
kn = knots(5,ni,mu, BigInt(0)//1,BigInt(1)//1);

m, dg = dim_deg(kn)

S1, I1  = g1matrix_edge(kn,N1,N2,
                        zeroOnEdge=false,
                        equalAcrossEdge=true);
K1 = nullspacex(Matrix(S1))

B1 = expand_spmatrix(K1,I1, m)
