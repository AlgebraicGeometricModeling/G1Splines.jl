include("../src/G1Splines.jl")
using SemiAlgebraicTypes, Axl

m = axlread("../data/ybranchcc1.axl")[1]

m[:color] = Axl.blue

msh = hmesh(m)

s = g1surface(msh)

@axlview s
