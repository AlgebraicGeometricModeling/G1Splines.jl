using DelimitedFiles
currentdir =  @__DIR__

include("../src/G1Splines.jl");

include("../src/fake_edges.jl")

meshname="cube";
fake_edge=true;

if (size(ARGS,1)>0)
    m = offread(ARGS[1]);
else
    m = offread(joinpath(currentdir,string("../data/",meshname,".off")));

end

    hm = hmesh(m);


    if fake_edge
        sharp_edges=readdlm(joinpath(currentdir,string("../data/cube_edges.txt")),Int);
        fake_edges(hm,sharp_edges)
    end

    divideEV(hm);

    @time begin

    res=g1surface(hm)

    #gsurf=res[2];


    @axlview res #gsurf #Plot the surface using Axl


    #basis=assemble_g1basis(gsurf);
    end
    return
    #ToGismo(gsurf,basis,gsurf.knots,meshname)
