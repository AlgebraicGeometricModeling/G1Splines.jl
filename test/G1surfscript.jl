"""
 Script to build the G1 surface
"""

currentdir =  @__DIR__

include(joinpath(currentdir,"../src/G1Splines.jl"));

meshname="el3";

#The input mesh must have EVs surrounded by just regular vertices; if this is not true, you must subdivide once (or more, for any reason).

if (size(ARGS,1)>0)
    m = offread(ARGS[1]);
else
    m = offread(joinpath(string(currentdir,"/../data/", meshname ,".off")));
end

m[:color] = Color(0,0,255);
hm = hmesh(m);
divideEV(hm); #Subdivide mesh with Catmull-Clark subdivision scheme if the mesh presents two adjacent EVs
nbevs(hm,1)

res=g1surface(hm); #G1 surface as a gspline structure.


