#Create a dictionary for INNER and BOUNDARY smoothing masks
#using FileIO
currentdir =  @__DIR__

include(joinpath(currentdir,"../src/G1Splines.jl"));

D=Dict{String, Dict{Int64, Matrix{Float64}}}(
    "CS-S"=>Dict(i=>G1matrix(i,"CS-S","INNER") for i=3:20),
    "CS-AS"=>Dict(i=>G1matrix(i,"CS-AS","INNER") for i=3:20),
    "NCS-S"=>Dict(i=>G1matrix(i,"NCS-S","INNER") for i=3:20),
    "NCS-AS"=>Dict(i=>G1matrix(i,"CS-S","INNER") for i=3:20),
    "BOUNDARY"=>Dict(i=>G1matrix(2*i,"CS-S","BORDEREV") for i=3:10)
)

save("EVsdict.jld2","EVsdict", D);
