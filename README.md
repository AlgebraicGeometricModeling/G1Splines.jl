# Julia functions to compute the G1 smoothing mask and the related G1 surface

To run the codes you need to install and include the following packages:

- LinearAlgebra (included in Julia's standard libraries)

- FileIO (included in Julia's standard libraries)

- JLD2 @v >= 0.4.22

- ToeplitzMatrices https://github.com/JuliaMatrices/ToeplitzMatrices.jl

- Axl https://gitlab.inria.fr/AlgebraicGeometricModeling/Axl.jl and https://gitlab.inria.fr/agm/axl.git

- DataStructures https://github.com/JuliaCollections/DataStructures.jl.git

- SemiAlgebraicTypes https://gitlab.inria.fr/AlgebraicGeometricModeling/SemiAlgebraicTypes.jl.git
