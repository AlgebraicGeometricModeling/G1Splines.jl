#/** @file gsMappedBasis_test.cpp

    @brief Example using the gsMappedBasis class

    This file is part of the G+Smo library.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    Author(s): A. Mantzaflaris
*/

//! [Include namespace]
#include <gismo.h>
//#include <gsPolynomial/gsMonomialBasis.h>

using namespace gismo;
//! [Include namespace]

#include <cfenv> //for float exceptions

// Fills in ones on the (permuted) diagonal of zero-columns of \a mat
void completeWithIdentity(gsSparseMatrix<> & mat)
{
    std::set<index_t> nzr;
    for (index_t k = 0; k!=mat.outerSize(); ++k)
        for(auto it = mat.begin(k); it; ++it)
            nzr.insert(it.row());
    nzr.insert(nzr.end(), mat.rows());
    index_t k = 0, c = mat.cols() - mat.rows() + nzr.size();
    for (const auto & s : nzr)
    {
        for (;k<s;++k)
            mat.insert(k,c++) = 1;
        k++;
    }
    mat.makeCompressed();
}

void PlotBasis(const gsSparseMatrix<> & cf, const gsMultiPatch<> & mp,
               const int & ns,
               const bool pm = false, const bool pn = false)
{
    //gsFileData<> xml;
    gsParaviewCollection col("ccbasis");
    gsMultiPatch<> mmp = mp;
    const index_t d = mp.parDim();
    gsInfo << "dim " <<d<<"\n";
    const size_t np = mp.nPatches();
    gsVector<index_t> offset(np+1);
    offset[0] = 0;
    for ( size_t i = 1; i< np; ++i)
    {
        offset[i] = offset[i-1] + mp.patch(i-1).basis().size();
        mmp.patch(i).embed(d+1);
    }
    offset[np] = offset[np-1] + mp.patch(np-1).basis().size();

    for ( index_t k = 0; k != cf.cols(); ++k) // for all basis function
    {
        if (cf.col(k).nonZeros()==1) continue; //skip free functions
        for ( size_t i = 0; i< np; ++i)
            mmp.patch(i).coefs().col(d).setZero();

        for(auto it = cf.begin(k); it; ++it)
        {
            index_t pid = std::upper_bound(offset.begin(),offset.end(),it.row())
                - offset.begin() - 1;
            index_t ii  = it.row() - offset[pid];
            mmp.patch(pid).coefs()(ii, d) = it.value();
        }

        // Export XML
        //xml << mmp;

        // Export Paraview
        std::string gcBasisFct = "ccbasis" + util::to_string(k);
        gsWriteParaview(mmp, gcBasisFct, ns, pm, pn);
        for ( size_t i = 0; i< np; ++i)
            col.addTimestep(gcBasisFct, i, k, ".vts");
        if(pm)
        {
            for ( size_t i = 0; i< np; ++i)
                col.addTimestep(gcBasisFct, i, k, "_mesh.vtp");
        }
        if(pn)
        {
            for ( size_t i = 0; i< np; ++i)
            col.addTimestep(gcBasisFct, i, k, "_cnet.vtp");
        }
    }
    col.save();
    //xml.save("representative_basis");
}



int main(int argc, char *argv[])
{

#ifndef _MSC_VER
    // Enable floating point exceptions
    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
#endif

    //! [Parse command line]
    bool bernstein = false, plot = false, plotbasis = false, polytest = false, prgeo=false;
    int numRefine  = 3;
    int qRule  = 1;
    int mode = 0;

    std::string fn("msplines/star3_");

    gsCmdLine cmd("Example using mapped spline bases.");
    cmd.addInt( "r", "uniformRefine", "Number of Uniform h-refinement steps to perform before solving",  numRefine );
    cmd.addString( "f", "file", "Input XML file prefix", fn );
    cmd.addInt("m", "mode", "Problem to solve", mode);
    cmd.addSwitch("plot", "Create a ParaView visualization file with the solution", plot);
    cmd.addSwitch("pbasis", "Create a ParaView visualization file with the basis", plotbasis);
    cmd.addSwitch("poly", "Test poly", polytest);
    cmd.addSwitch("geo", "Project geometry", prgeo);
    cmd.addSwitch("bb", "Use Bernstein basis", bernstein);
    //cmd.addInt( "q", "quadrature", "1 or 2", qRule );

    cmd.getValues(argc,argv);
    //! [Parse command line]
    
    //! [Problem setup]
    int init_knots = 0;
    gsFileData<> fd;
    gsMultiPatch<> mp;
    gsMultiBasis<> mb;
    gsSparseMatrix<> cf;

    gsExprAssembler<> A(1,1);
    gsExprEvaluator<> ev(A);
    //ev.options().setReal("quA", 2);// increase norm quadrature accuracy
    ev.options().addInt("quRule","",2); // 1:Gauss-Legendre, 2:Gauss-Lobatto
    //A.options().addInt("quRule","", qRule); // 1:Gauss-Legendre, 2:Gauss-Lobatto
    //A.options().addInt("quB","", 1);
    A.setIntegrationElements(mb);
    A.options().addInt("quRule","",2); // 1:Gauss-Legendre, 2:Gauss-Lobatto
    A.options().addInt("quB","", 3);

    gsInfo<<"Active options:\n"<< A.options() <<"\n";
    typedef gsExprAssembler<>::geometryMap geometryMap;
    typedef gsExprAssembler<>::variable    variable;
    typedef gsExprAssembler<>::space       space;
    typedef gsExprAssembler<>::solution    solution;

    // Set the discretization space
    short_t d = 2; //mb.domainDim();
    gsMappedBasis<2,real_t> bb2;
    if (bernstein) {fd.read(fn + "0.xml"); fd.getFirst(mb);}
    space u = (bernstein ? A.getSpace(mb) : A.getSpace(bb2) );

    // Get the map and function
    geometryMap G = A.getMap(mp);
    //gsFunctionExpr<> ff("t:=5; cos(t*pi*x) * sin(t*pi*y)", 2);
    //gsFunctionExpr<> ff("cos(pi*x) * 1e1", 2);
    //gsFunctionExpr<> ff("1+(2*x)^11*(1.4y)^12", 2);
    gsFunctionExpr<> ff("2*cos(2*x)*sin(2*y)",2);
    gsFunctionExpr<> lapff("t:=1;-cos(t*pi*x) * sin(t*pi*y) * 2 * t^2 * (pi^2)", 2);
    gsFunctionExpr<> bilapff("t:=1;cos(pi*x) * sin(pi*y) * 4 * t^4 * (pi^4)", 2);

    auto f = A.getCoeff(ff, G);
    auto lf = A.getCoeff(lapff, G);
    auto blf = A.getCoeff(bilapff, G);

    // Solution vector and solution variable
    gsMatrix<> sVector;
    solution s = A.getSolution(u, sVector);

    // Boundary conditions
    gsBoundaryConditions<> bc;
    bc.setGeoMap(G.source());

    //! [Solver loop]
//#   ifdef GISMO_WITH_PARDISO
//    gsSparseSolver<>::PardisoLDLT solver;
//#   else
    gsSparseSolver<>::BiCGSTABILUT solver;
    //solver.setTolerance(1e-16);
    //gsInfo<<"CG Tolerance: "<< solver.tolerance() <<"\n";
//#   endif

    gsVector<> l2err(numRefine+1), h1err(numRefine+1),
        h2err(numRefine+1), linferr(numRefine+1), CGres(numRefine+1);
    gsVector<index_t>  CGiter(numRefine+1);
    gsInfo<< "(dot1=assembled, dot2=solved, dot3=got_error)\n"
        "\nDoFs: ";

    for( index_t r = 0; r<=numRefine; ++r)
    {
        //fd.read(fn + util::to_string(init_knots++) + ".xml.gz");
        if ( !fd.read(fn + util::to_string(init_knots++) + ".xml") )
        {
            gsInfo <<"ERROR: file not found ["<<fn + util::to_string(init_knots++) + ".xml]\n";
            return EXIT_FAILURE;
        }

        fd.getFirst(mp);
        if (bernstein)
        {
            mp.computeTopology();
            mp.closeGaps();
        }
        fd.getFirst(mb);
        mb.setTopology(mp);
        //mb.degreeReduce(2);
        fd.getFirst(cf);
        //completeWithIdentity(cf);

        //gsInfo <<"\nRank: "<< cf.toDense().fullPivLu().rank() <<"\n";

        if (plotbasis)
        {
            if (r!=numRefine) continue;
            PlotBasis(cf,mp,1000,false,false); //last false is for the net
            gsFileManager::open("ccbasis.pvd");
            return 0;
        }

        mp.embed(2);// 2D example !
        bb2.init(mb,cf);

/*
        if (polytest)
        {
            gsMonomialBasis<real_t> pl1(2);//check linear precision
            gsGenericTensorBasis<2,real_t> pl(pl1,pl1);
            for (index_t i = 0; i!=pl.size(); ++i)
            {
                auto bf  = pl.function(i);
                auto mon = A.getCoeff(bf, G);
                auto ti  = pl.tensorIndex(i);
                u.setup();
                A.initSystem();
                A.assemble(  u * u.tr(), u * mon );
                solver.compute( A.matrix() );
                sVector = solver.solve(A.rhs());
                real_t err = math::sqrt(ev.integral((mon - s).sqNorm()*meas(G)));
                gsInfo <<"Monomial x^"<<ti[0]<<"y^"<<ti[1]<<", err="
                       <<err<<( err<1e-9 ? "----- OK\n":"\n");
            }

            return EXIT_SUCCESS;
        }
        */
        if (prgeo)
        {
            gsMappedSpline<2,real_t> ms(mp,cf);
            //gsWrite(ms,"g1spline");//todo
            auto G1 = A.getMap(ms);
            real_t err = math::sqrt(ev.integral((G1 - G).sqNorm()));
            gsInfo <<"Geometry error: "<< err <<"\n";
            return EXIT_SUCCESS;
        }

        // Set Dirichlet boundary conditions on all boundaries
        bc.clear();
        for ( gsMultiPatch<>::const_biterator bit = mp.bBegin();
              bit != mp.bEnd(); ++bit)
            bc.addCondition( *bit, condition_type::dirichlet, &ff );

        if (mode == 0)
            u.setup(0); // if no BCs needed
        else
        {
            u.setup(bc, dirichlet::l2Projection, 0);
        }

        // Initialize the system
        A.initSystem();

        gsInfo<< A.numDofs() <<std::flush;

        // Compute the system matrix and right-hand side
        switch (mode) {
        case 0:
            A.assemble(u * u.tr(), u * f);
            break;
        case 1:
            // Poisson
            A.assemble(igrad(u, G) * igrad(u, G).tr() * meas(G), -u * ilapl(f) * meas(G));
            break;
        case 2:
            // Biharmonic
            //A.assemble(ilapl(u, G) * ilapl(u, G).tr() * meas(G), u * ibilapl(f) * meas(G));
            // Neumann term
            A.assembleBdr(bc.get("Dirichlet"), igrad(u, G) * nv(G) * lf);
            break;
        }

        //gsInfo <<"\nInv: "<< A.matrix().toDense().fullPivLu().isInvertible() <<"\n";
        //gsInfo <<"\nDet: "<< A.matrix().toDense().fullPivLu().determinant() <<"\n";
        
        gsInfo<< "." <<std::flush;// Assemblying done
        //solver.setMaxIterations(3*A.matrix().rows());
        solver.compute( A.matrix() );
        sVector = solver.solve(A.rhs());
        CGiter[r] = 0;//solver.iterations();
        CGres[r] = 0;//solver.error();
        if (solver.info()!=Eigen::Success)
            gsInfo<< "! Solver did not converge"<<"\n"; // Linear solving done

        gsInfo<< "." <<std::flush; // Linear solving done

        l2err[r]= math::sqrt( ev.integral( (f - s).sqNorm()*meas(G) ) / ev.integral(f.sqNorm()*meas(G)) );

        h1err[r]= l2err[r] + math::sqrt(ev.integral( ( igrad(f) - grad(s)*jac(G).inv() ).sqNorm()*meas(G) ) /ev.integral( igrad(f).sqNorm()*meas(G) ) );

        h2err[r]=// h1err[r] +
            math::sqrt(
                ev.integral( ( ihess(f) - ihess(s,G) ).sqNorm()*meas(G) )
                //       /ev.integral( ihess(f).sqNorm()*meas(G) )
                );
        linferr[r] = ev.max( abs(f.val()-s.val()) ) / ev.max(f);

        gsInfo<< ". " <<std::flush; // Error computations done
    }
    //! [Solver loop]

    // gsInfo<< "\n\nCG it. : "<<CGiter.transpose()<<"\n";
    // gsInfo<<     "CG res.: "<<CGres.transpose()<<"\n";

    //! [Error and convergence rates]
    gsInfo<<"\n* Error\n";
    gsInfo<< "H2    "<<std::scientific<<h2err.transpose()<<"\n";
    gsInfo<< "H1    "<<std::scientific<<h1err.transpose()<<"\n";
    gsInfo<< "L2    "<<std::scientific<<std::setprecision(3)<<l2err.transpose()<<"\n";
    gsInfo<< "Linf  "<<std::scientific<<linferr.transpose()<<"\n";

    gsInfo<<"\n* EoC\n";

    gsInfo<< "H2c   0 "<< std::fixed<<std::setprecision(2)
          <<( h2err.head(numRefine).array() /
              h2err.tail(numRefine).array() ).log().transpose() / std::log(2.0) <<"\n";
    gsInfo<< "H1c   0 "<< std::fixed<<std::setprecision(2)
          <<( h1err.head(numRefine).array() /
              h1err.tail(numRefine).array() ).log().transpose() / std::log(2.0) <<"\n";
    gsInfo<< "L2c   0 " << std::fixed<<std::setprecision(2)
          << ( l2err.head(numRefine).array() /
               l2err.tail(numRefine).array() ).log().transpose() / std::log(2.0) <<"\n";

    gsInfo<<   "Linfc 0 "<< std::fixed<<std::setprecision(2)
          <<( linferr.head(numRefine).array() /
              linferr.tail(numRefine).array() ).log().transpose() / std::log(2.0) <<"\n";

    //! [Error and convergence rates]

    //! [Export visualization in ParaView]
    if (plot)
    {
        gsInfo<<"Plotting in Paraview...\n";
        ev.options().setSwitch("plot.elements", true);
        ev.options().setInt   ("plot.npts"    , 300);
        ev.writeParaview( s, G, "solution");
        //ev.writeParaview( grad(s), G, "solution_grad");
        //ev.writeParaview( grad(f), G, "solution_ex_grad");
        ev.writeParaview( (f-s), G, "error_pointwise");
        ev.options().setSwitch("plot.elements", true);
        ev.writeParaview( f, G, "solution_ex");                
        gsFileManager::open("error_pointwise.pvd");
    }
    //! [Export visualization in ParaView]

    return EXIT_SUCCESS;

}// end main
