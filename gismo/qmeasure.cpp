
#include <gismo.h>

#include <cfenv> //for float exceptions

using namespace gismo;

int main(int argc, char *argv[])
{
    // Enable floating point exceptions
    #ifndef _MSC_VER
    // Enable floating point exceptions
        feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
    #endif

    std::string fn("");
    bool verbose = false;
    bool plot = false;
    gsCmdLine cmd("Compute normals.");
    cmd.addPlainString("filename", "File containing geometry data.", fn);
    cmd.addSwitch("verbose", "Show result and exact", verbose);
    cmd.addSwitch("plot", "Plot normals", plot);
    try { cmd.getValues(argc,argv); } catch (int rv) { return rv; }

    gsMultiPatch<> mp;
    if (!fn.empty())
    {
        gsReadFile<>(fn, mp);
        if (mp.nInterfaces() == 0)
            mp.computeTopology();
        //mp.closeGaps();//closing gaps requires same degree on interfaces
        gsInfo<< "Got "<< mp <<"\n";
        mp.embed(3);
    }

    //mp.closeGaps();

    gsMultiBasis<> basis(mp);
    gsExprEvaluator<> ev;
    ev.setIntegrationElements(basis);

    auto G = ev.getMap(mp);

    ev.integralInterface( ( G.left() - G.right() ).sqNorm() );
    ev.calcSqrt();
    bool ok = (ev.value()<1e-3);
    if (ok) gsInfo  <<"\nSurface is C0.";
    gsInfo  <<"\nResult (C0): "<< ev.value() <<"\n";
    if (verbose)
        gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";//<<"\n";

    ev.integralInterface( (sn(G.left()).normalized()-sn(G.right()).normalized()).sqNorm() );
    ev.calcSqrt();
    ok &= (ev.value()<1e-3);
    if (ok) gsInfo  <<"\nSurface is G1.";
    gsInfo  <<"\nResult (G1): "<< ev.value() <<"\n";
    if (verbose)
        gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";//<<"\n";
    ev.integralInterface( (sn(G.left())-sn(G.right())).sqNorm() );
    ev.calcSqrt();
    ok &= (ev.value()<1e-3);
    if (ok) gsInfo  <<"\nSurface has continuous normal.";
    gsInfo  <<"\nResult (non-unit normal): "<< ev.value() <<"\n";

/*
    ev.maxInterface( expr::abs( jac(G.left()).norm()-jac(G.right()).norm() ) );
    gsInfo  <<"Result (jac. norm diff): "<< ev.value() <<"\n";
    if (verbose)
        gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";//<<"\n";

    ev.writeParaview(abs( jac(G).sqNorm() ),              G, "jacnorm");
//    ev.writeParaview( jac(G),              G, "jacnorm");
//*/

    ev.maxInterface( abs( (fform(G.left() ).inv()*fform2nd(G.left() )).det() -
                          (fform(G.right()).inv()*fform2nd(G.right())).det() ) );
    ok &= (ev.value()<1e-3);
    if (ok) gsInfo  <<"\nSurface has continuous Gauss curvature.";
    gsInfo  <<"\nResult (Gauss curv. diff): "<< ev.value() <<"\n";
    if (verbose)
        gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";//<<"\n";




    ev.maxInterface( abs( (fform(G.left() ).inv()*fform2nd(G.left() )).trace().val() -
                          (fform(G.right()).inv()*fform2nd(G.right())).trace().val() ) );
    ok &= (ev.value()<1e-3);
    if (ok) gsInfo  <<"\nSurface has continuous mean curvature.";
    gsInfo  <<"\nResult (mean curv. diff): "<< ev.value() <<"\n";
    if (verbose)
        gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";//<<"\n";


    if (plot)
    {
        gsInfo<<"Plotting...\n";
        ev.writeParaview(sn(G),              G, "normal");
        ev.writeParaview(sn(G).normalized(), G, "unit_normal");
        ev.writeParaview(hess(G).sqNorm(), G, "hess_norm");
    }

    ev.minInterface( (sn(G.left()).normalized()+sn(G.right()).normalized()).norm()/2.0 );
    if (ev.value()<1e-3) gsInfo <<"\nSurface is NOT oriented properly.";
    else gsInfo <<"\nSurface is oriented.";
    gsInfo  <<"\nResult (orient): "<< ev.value() <<"\n";
    if (verbose)
        gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";//<<"\n";

    //test vertices
    ev.options().setReal("quA",0);
    ev.options().setInt ("quB",2);
    ev.options().addInt ("quRule","",gsQuadrature::GaussLobatto); //Gauss-Lobatto

    ev.maxInterface( (G.left()-G.right()).norm() );
    gsInfo  <<"\nResult (vertex values diff): "<< ev.value() <<"\n";
    if (verbose)
        gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";//<<"\n";

    ev.maxInterface( (sn(G.left()).normalized()-sn(G.right()).normalized()).norm());
    gsInfo  <<"Result (vertex unit normals diff): "<< ev.value() <<"\n";
    if (verbose) gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";

    ev.maxInterface( (sn(G.left())-sn(G.right()) ).norm() );
    gsInfo  <<"Result (vertex normals diff): "<< ev.value() <<"\n";
    if (verbose) gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";

    /*
    ev.maxInterface( ( hess(G.left())-hess(G.right())).sqNorm() );
    ev.calcSqrt();
    ok &= (ev.value()<1e-3);
    gsInfo  <<"\nResult (hessian norm diff): "<< ev.value() <<"\n";
    //if (verbose) gsInfo<<"Per edge: "<<ev.allValues().transpose() <<"\n";
    */

    return EXIT_SUCCESS;
}



//gsDebugVar(ev.max(  G.left()-G.right() ) ); //should fail..
