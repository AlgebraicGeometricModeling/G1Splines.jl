using Documenter
using G1Splines

dir = "mrkd"

#Expl = map(file -> joinpath("expl", file),
#           filter(x ->endswith(x, ".md"), readdir(dir*"/expl")))

makedocs(
    sitename = "G1Splines",
    authors = "M. Marsala, B. Mourrain",
    modules = [G1Splines],
    build = "GSplines.jl/docs",
    source = "mrkd",
    pages = Any[
        "Home" => "index.md",
        "Functions & types" => [
            "geometric_model.md",
            "converters.md",
            "iga.md"
        ],
        "Using G+Smo" => "gismo.md",
        "About the package"  => "package.md",
    ],
    repo = "https://gitlab.inria.fr/AlgebraicGeometricModeling/G1Splines.jl",
    doctest = false,
)

deploydocs(
#    deps = Deps.pip("pygments", "mkdocs", "python-markdown-math"),
    repo = "gitlab.inria.fr/AlgebraicGeometricModeling/GI1splines.jl.git"
)
