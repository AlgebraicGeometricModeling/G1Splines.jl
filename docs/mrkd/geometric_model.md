# Geometric models 
```@index
Pages = ["geometric_models.md"]
```

```@docs 
g0surface
```

```@docs 
g1surface
```

```@docs 
G1Splines.g1matrix
```

```@docs 
G1Splines.G1matrix
```

```@docs 
G1Splines.g1matrix_edge
```

