# About MomentTools.jl



## Installation

The package can be installed from Julia as follows:

```
] add https://gitlab.inria.fr/AlgebraicGeometricModeling/G1Splines.jl

```

A version of Julia >= 1.3 should be used.

It depends on the following packages, which are installed automatically:

  - `SemiAlgebraicTypes`
    
    
## Development

The git project of the package is
    [https://gitlab.inria.fr/AlgebraicGeometricModeling/G1Splines.jl](https://gitlab.inria.fr/AlgebraicGeometricModeling/G1Splines.jl).
    
The main developpers are (so far)

  - Michelangelo Marsala
  - Bernard Mourrain

