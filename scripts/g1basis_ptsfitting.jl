#This code generates the G1 Bézier biquintic basis functions on a domain parametrized by the G1ACC scheme 
#obtained from an input quad mesh and export them in .xml format to be used in G+Smo codes

using G1Splines

currentdir =  @__DIR__

#include(joinpath(currentdir,"../src/G1Splines.jl"));

#The input mesh must have EVs surrounded by just regular vertices; if this is not true, the code subdivide it once (or more, for any reason).

meshname="triangle_planar";

if (size(ARGS,1)>0)
    m = offread(ARGS[1]);
else
    m = offread(joinpath(currentdir,string("../data/off_Meshes/",meshname,".off")));
end

gsurf=g1surface(m)
basis=g1basis_bezier(m);

ToGismo(gsurf,basis,gsurf.knots,meshname)

return basis
