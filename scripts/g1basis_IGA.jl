#using GSplines
currentdir =  @__DIR__

include(joinpath(currentdir,"../src/G1Splines.jl"));

#dir = joinpath(currentdir,"../Data/IGA Meshes");

#F = filter(x -> endswith(x, ".off") , readdir(dir))

plot=false
meshname="penta";
nbiter=1

if (size(ARGS,1)>0)
    m = offread(ARGS[1]);
else
    m = offread(joinpath(currentdir,string("../data/IGA_Meshes/",meshname,".off")));
end


        hm = hmesh(m);
        divideEV(hm);

        for i in nbiter

                knt=knots(5,2^i,5)

                if plot
                   nbpts,d=dim_deg(vec(knt))
                   d=nbpts-1
                   rescc=acc_d(hm,d); #Collection of spline patches #res=[sup,gs]
                   gsurfcc=rescc;
                else
                   res=g1surface(hm)
                   gsurfcc=res
                end

                basis=assemble_g1basis_gluing(hm,vec(knt),i);
                #basis=assemble_g1basis_spline(hm,vec(knt));
                #basis=assemble_g1bais_gluing(hm,vec(knt),i);
                #G=g1dimension(hm,knt);
                #B=G[3]
                #basisker=nullspace(Matrix(B));
                #basis=sparse(basisker)

                v=ones(size(basis)[1],1);
                println("Nb of linearly DEPENDENT basis: ", size(basis,2)-rank(basis));
                C=basis\v;

                println("Unit function error: " , maximum(abs.(vec(basis*C-v))))
                if !plot ToGismo(gsurfcc,basis,vec(knt),string(meshname,"_BASIS_BOTH_EXPL_PLOT_",i)) end ;
                println(" G1DIM_MATRIX: ", size(basis,2), " G1DIM_FUN: ", g1dimension(hm,vec(knt))[1])


                if plot
                    for i in 40:size(basis)[2]
                        row=findnz(basis[:,i])[1];
                        val=findnz(basis[:,i])[2];
                        local gsbas = GSpline(0, knt,hm )
                        CP=copy(gsurfcc.ctrpoints);
                        for j in 1:length(row)
                            CP[3,row[j]]=val[j];
                        end
                        gsbas.ctrpoints=CP;
                        @axlview gsbas
                    end
                end


        end
